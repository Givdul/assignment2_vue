# Trivia Vue

The second assignment in the front end part of Noroff's Accelerate bootcamp is a Trivia game where you answer 10 questions and try to get a good score.

[Component tree](https://www.figma.com/file/CIvTMyLIOWefBevLwcaVXC/Component-Tree?node-id=0%3A1)

## Installation

* Clone down the repository
* ``$npm run dev ``
* go to localhost url

## Usage

* Enter your username
* Answer the questions.
* When done press Logout button to clear localstorage

## Authors
[Ludvig Hansen](https://gitlab.com/Givdul) and [Martin Kanestrøm](https://gitlab.com/Martinkanestrom)

## License
[MIT](https://choosealicense.com/licenses/mit/)
