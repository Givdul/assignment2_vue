import { QUESTION_API } from ".";

export interface Questions {
    category: string,
    type: string,
    question: string,
    correct_answer: string,
    incorrect_answers: Array<string>
}

export interface Answers {
    correct_answer: string,
    incorrect_answers: Array<string>
}


export const getQuestions = async (): Promise<[string | null, Questions[]]> => {
    
    try {
        const response = await (await fetch(QUESTION_API)).json();
        console.log([[],response.results])
        return await [null, response.results]; 
    } catch(error: any) {
        return[error.message, []]
    }
}

export const randomizeAnswers = async (qs: Array<Answers>, idx: number) => {
    let answersArray: Array<any> = [qs[idx].correct_answer, ...qs[idx].incorrect_answers];
    let currIdx = answersArray.length, randomIdx;
    // While there are elements to shuffle
    while(currIdx != 0) {
        randomIdx = Math.floor(Math.random() * currIdx);
        currIdx--;
        // Swap elements with the current element
        [answersArray[currIdx], answersArray[randomIdx]] = [answersArray[randomIdx], answersArray[currIdx]];
    }
    return await answersArray;
}