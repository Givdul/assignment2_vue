import { BASE_URL, API_KEY as apiKey } from ".";

export async function apiUserRegister(username: string) {
    try {
        const config = {
            method: "POST",
            headers : {
                "X-API-Key": apiKey, 
                "content-type": "application/json"
            },
            body: JSON.stringify({
                username: username,
                highScore: 0 
    
            }) 
        }

        const response = await fetch(`${BASE_URL}`, config)
        const { data } = await response.json()
        return [ null, data ]
        
    }
    catch(error: any){//usikker på type her, men får error uten
        return [ error.message, null ]
    }
}

export const getUserInfo = async (username: string) => {
    try {
        const response = await (await fetch(`${BASE_URL}?username=${username}`)).json();
        return await response;
    } catch(error: any) {
        return error.message;
    }
}

export async function patchUserScore(id: number, score: number) {
    fetch(`${BASE_URL}/${id}`, {
        method: 'PATCH',
        headers: {
            'X-API-Key': apiKey,
          'Content-Type': "application/json"
        },
        body: JSON.stringify({
            highScore: score 
        })
    })
    .then(response => {
      if (!response.ok) {
        throw new Error("Could not update high score")
      }
      return response.json()
    })
    .then(updatedUser => {

    })
    .catch(error => {
    })
}
