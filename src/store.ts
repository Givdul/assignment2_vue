import { InjectionKey } from "vue";
import { createStore , useStore as baseUseStore, Store, createLogger} from "vuex";
import { Questions } from "./api/questions";

export interface State {
    questions: Questions[],
    user: string
}

const key: InjectionKey<Store<State>> = Symbol();

export default createStore ({
    state: {
        questions: [],
        user: ""
    },
    mutations: {
        setQuestions: (state: State, payload: Questions[]) => {
            state.questions = [...payload];
        },
    },
    actions: {

    }
})

export function useStore () {
    return baseUseStore(key)
}