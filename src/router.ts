import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import start from "./views/start.vue";
import question from "./views/question.vue";
import result from "./views/result.vue";

const routes: RouteRecordRaw[] = [
    {
        path: "/",
        component: start

    },
    {
        path: "/questions",
        component: question
    },
    {
        path: "/result",
        component: result
    }
]

export default createRouter ({
    history: createWebHistory(),
    routes
})